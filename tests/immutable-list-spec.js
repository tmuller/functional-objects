/**
 * Unit tests for the immutable list
 */

'use strict';

var iList = require('../lib/immutable-list');
var optionPkg = require('../lib/option');

describe('Immutable List', function() {

  describe('constructor', function() {

    it('should create an empty list if no parameters are passed', function() {
      var l = new iList();
      expect(l.head).toEqual(null);
    });

    it('should populate the list with the provided arguments', function() {
      var l = new iList(42, 12, 45, 33);
      expect(l.head.get()).toEqual(42);
      expect(l.head.next().get()).toEqual(12);
      expect(l.head.next().next().get()).toEqual(45);
      expect(l.head.next().next().next().get()).toEqual(33);
      expect(l.head.next().next().next().next()).toEqual(null);
    });

    it('should be instantiatable without the "new" keyword', function() {
      var l = iList(42, 12, 45, 33);
      expect(l.head.get()).toEqual(42);
      expect(l.head.next().get()).toEqual(12);
      expect(l.head.next().next().get()).toEqual(45);
      expect(l.head.next().next().next().get()).toEqual(33);
      expect(l.head.next().next().next().next()).toEqual(null);
    });

    it('should set the length of the list', function() {
      var list = iList(1,2,3,4);
      expect(list.length).toEqual(4);
    });

    it('should be able to be instantiated with apply method', () => {
      const initialValues = [1,45,104,28,154,117];
      var l = iList.apply(null, initialValues);
      expect(l.head.get()).toEqual(1);
      expect(l.head.next().get()).toEqual(45);
      expect(l.toArray()).toEqual(initialValues);
    });
  });

  describe('fromArray', function() {

    function assertEmptyList(listRef) {
      expect(listRef.toArray()).toEqual([]);
      expect(listRef.next).toEqual(null);
    }

    it('should generate a List from an array', () => {
      const initialValues = [1,45,104,28,154,117];
      var l = new iList().fromArray(initialValues);
      expect(l.toArray()).toEqual(initialValues);
      expect(l.head.get()).toEqual(1);
      expect(l.head.next().get()).toEqual(45);
      expect(l.head.next().next().get()).toEqual(104);
    });

    it('should generate an empty List from a number type', () => {
      var l = new iList().fromArray(5);
      assertEmptyList(l);
    });

    it('should generate an empty List from a string type', () => {
      var l = new iList().fromArray('here is a string');
      assertEmptyList(l);
    });

    it('should generate an empty List from an object type', () => {
      var l = new iList().fromArray({});
      assertEmptyList(l);
    });

    it('should generate an empty List from a function type', () => {
      let testValue = 42;
      const initialValue = function() { testValue = 153 }.bind(this);
      var l = new iList().fromArray(initialValue);
      assertEmptyList(l);
      expect(testValue).toEqual(42); // assuring function wasn't executed
    });

    it('should generate an empty List when not passing a parameter', () => {
      var l = new iList().fromArray();
      assertEmptyList(l);
    });

    it('should generate an empty List when passing null', () => {
      var l = new iList().fromArray(null);
      assertEmptyList(l);
    });

  });

  describe('toArray', function() {

    it('should convert a list to an array', function() {
      var l = new iList('test', [], 42, null, {});
      var result = l.toArray();
      expect(result).toEqual(['test',[],42,null,{}])
    });

    it('should work on an empty list', function() {
      var l = new iList();
      var result = l.toArray();
      expect(result).toEqual([]);
    });

  });

  describe('insertHead', function() {

    it('should create a new list with new node as the head', function() {
      var l = iList(42,15);
      var newList = l.insertHead(33);
      expect(newList.head.get()).toEqual(33);
      expect(newList.head.next().get()).toEqual(42);
      expect(newList.head.next().next().get()).toEqual(15);
      expect(newList.head.next().next().next()).toEqual(null);
    });

    it('should leave the previous list unchanged', function() {
      var l = iList(42,15);
      var newList = l.insertHead(33);

      expect(newList.head.get()).toEqual(33);
      expect(newList.head.next().get()).toEqual(42);
      expect(newList.head.next().next().get()).toEqual(15);
      expect(newList.head.next().next().next()).toEqual(null);

      expect(l.head.get()).toEqual(42);
      expect(l.head.next().get()).toEqual(15);
      expect(l.head.next().next()).toEqual(null);
    });

    it('should populate en empty list', function() {
      var l = iList();
      expect(l.head).toEqual(null);
      var result = l.insertHead(42);
      expect(result.head.get()).toEqual(42);
      expect(result.head.next()).toEqual(null);
    });

    it('should set the length property', function() {
      var l = iList(42,15);
      var newList = l.insertHead(33);
      expect(newList.length).toEqual(3);
    });

  });

  describe('map', function() {

    var list;
    beforeEach(function() {
      list = new iList(1,2,3,4,5,6,7,8,9,10,12,14,16,18);
    });

    it('should apply the function to each element', function() {

      var tstFct = function(x) { return x * 2 };
      var result = list.map(tstFct);
      expect(result.toArray()).toEqual([2,4,6,8,10,12,14,16,18,20,24,28,32,36]);
    });

    it('should set the length on the new list appropriately', function() {
      var tstFct = function(x) { return x * 2 };
      var result = list.map(tstFct);
      expect(result.length).toEqual(14);
    });

    it('should not fail on an empty list', function() {
      var tstFct = function(x) { return x * 2 };
      var l = new iList();
      var result = l.map(tstFct);
      expect(result.head).toEqual(null);
      expect(result.length).toEqual(0);
    });

    it('should not modify the source list', function() {
      var tstFct = function(x) { return x * 2; }
      var result = list.map(tstFct);
      expect(list.toArray()).toEqual([1,2,3,4,5,6,7,8,9,10,12,14,16,18]);
    });

    // xit('should handle errors', function() {});


  });

  describe('filter', function() {

    var list;
    beforeEach(function() {
      list = new iList(1,2,3,4,5,6,7,8,9);
    });

    it('should correctly filter the list', function() {
      var filterFct = function(x) { return x % 2 === 0 };
      var filtered = list.filter(filterFct);
      expect(filtered.toArray()).toEqual([2,4,6,8]);
    });

    it('should not modify the original list', function() {
      var filterFct = function(x) { return x % 2 === 0 };
      var filtered = list.filter(filterFct);
      expect(filtered.toArray()).toEqual([2,4,6,8]);
      expect(list.toArray()).toEqual([1,2,3,4,5,6,7,8,9]);
    });

    it('should set the length of the list', function() {
      var filterFct = function(x) { return x % 2 === 0 };
      var filtered = list.filter(filterFct);
      expect(filtered.length).toEqual(4);
    });

  });

  describe('drop', function() {

    var list;
    beforeEach(function() {
      list = new iList(1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19);
    });

    it('should drop the specified number of elements', function() {
      var result = list.drop(5);
      expect(result.toArray()).toEqual([6,7,8,9,10,11,12,13,14,15,16,17,18,19]);
    });

    it('should not modify the source list', function() {
      var result = list.drop(15);
      expect(result.toArray()).toEqual([16,17,18,19]);
      expect(result === list).toEqual(false);
      expect(list.toArray()).toEqual([1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19]);
    });

    it('should set the length of the new list', function() {
      var result = list.drop(13);
      expect(result.length).toEqual(6);
    });

    it('should return an empty list if an empty list is passed', function() {
      var l = new iList();
      var result = l.drop(15);
      expect(result.length).toEqual(0);
      expect(result.head).toEqual(null);
      expect(result.last).toEqual(null);
    });

    it('should return a different empty list if an empty list is passed', function() {
      var l = new iList();
      var result = l.drop(15);
      expect(l === result).toEqual(false);
    });

    it('should return an empty list if there are fewer elements than drops', function() {
      var result = list.drop(25);
      expect(result.length).toEqual(0);
      expect(result.head).toEqual(null);
      expect(result.last).toEqual(null);
    });

    it('should return an empty list if the number of element is equal to the number of drop', function() {
      var result = list.drop(list.length);
      expect(result.length).toEqual(0);
      expect(result.head).toEqual(null);
      expect(result.last).toEqual(null);
    });
  });

  describe('dropWhile', function() {

    var list;
    beforeEach(function() {
      list = new iList(1,2,3,4,5,6,7,8,9,0,12,13,14,15,16,17,8,67);
    });

    it('should drop elements as long as predicate returns true', function() {
      var predicate = function(x) { return x > 0;};
      var result = list.dropWhile(predicate);
      expect(result.toArray()).toEqual([0,12,13,14,15,16,17,8,67]);
    });

    it('should not modify the original list', function() {
      var predicate = function(x) {return x > 0;};
      var result = list.dropWhile(predicate);
      expect(result.toArray()).toEqual([0,12,13,14,15,16,17,8,67]);
      expect(list.toArray()).toEqual([1,2,3,4,5,6,7,8,9,0,12,13,14,15,16,17,8,67]);
    });

    it('should return a new empty list if passed an empty list', function() {
      var l = new iList();
      var predicate = function(x) {return x > 0;};
      var result = l.dropWhile(predicate);
      expect(result.length).toEqual(0);
      expect(result.head).toEqual(null);
      expect(result.last).toEqual(null);
      expect(result === l).toEqual(false);
    });

    it('should return an empty list with a predicate that always returns true', function() {
      var predicate = function(x) {return true;};
      var result = list.dropWhile(predicate);
      expect(result.head).toEqual(null);
      expect(result.last).toEqual(null);
      expect(result.length).toEqual(0);
      expect(list.toArray()).toEqual([1,2,3,4,5,6,7,8,9,0,12,13,14,15,16,17,8,67]);
    });

    it('should set the length of the new list correctly', function() {
      var predicate = function(x) {return x > 0;};
      var result = list.dropWhile(predicate);
      expect(result.length).toEqual(9)
    });

    it('should stop dropping at the first predicate failure', function() {
      var l = new iList(12,3,4,8,0,34,7,3,-1,4);
      var predicate = function(x) {return x > 0;};
      var result = l.dropWhile(predicate);
      expect(result.length).toEqual(6);
      expect(result.toArray()).toEqual([0,34,7,3,-1,4]);
    });

  });

  describe('exists', function() {

    var list = new iList(1,2,3,4,5,6,7);

    it('should return true of the requested element exists', function() {
      var predicate = function(x) { return x > 5; }
      expect(list.exists(predicate)).toEqual(true);
    });

    it('should return false if the requested element does not exist', function() {
      var predicate = function(x) { return x > 55; }
      expect(list.exists(predicate)).toEqual(false);
    });
  });

  describe('toString', function() {

    it('should create a proper string for numbered list', function() {
      var list = new iList(1,2,3,4,5);
      var result = list.toString();
      expect(result).toEqual('1,2,3,4,5');
    });

  });

  describe('forEach', function() {

    var list;
    beforeEach(function() {
      list = iList(1,5,3,8,2,67);
    });

    it('should apply the function to every element in the list', function() {
      var result = [];
      var tstFct = function(x) { result.push(x * 3); };
      list.forEach(tstFct);
      expect(result).toEqual([3,15,9,24,6,201]);
    });

    it('should not modify the original list', function() {
      var tstFct = function(x) { return x * 3; };
      var result = list.forEach(tstFct);
      expect(list.head.get()).toEqual(1);
      expect(list.head.next().get()).toEqual(5);
      expect(list.head.next().next().get()).toEqual(3);
      expect(list.head.next().next().next().get()).toEqual(8);
      expect(list.head.next().next().next().next().get()).toEqual(2);
      expect(list.head.next().next().next().next().next().get()).toEqual(67);
      expect(list.head.next().next().next().next().next().next()).toEqual(null);

    });


  });

  describe('removeHead', function() {

    var list;
    beforeEach(function() {
      list = iList(1,5,3,8,2,67);
    });

    it('should create a shortened list', function() {
      var result = list.removeHead();
      expect(result.head.get()).toEqual(5);
      expect(result.head.next().get()).toEqual(3);
      expect(result.head.next().next().get()).toEqual(8);
      expect(result.head.next().next().next().get()).toEqual(2);
      expect(result.head.next().next().next().next().get()).toEqual(67);
    });

    it('should leave the original list unchanged', function() {
      var result = list.removeHead();
      expect(list.head.get()).toEqual(1);
      expect(list.head.next().get()).toEqual(5);
      expect(list.head.next().next().get()).toEqual(3);
      expect(list.head.next().next().next().get()).toEqual(8);
      expect(list.head.next().next().next().next().get()).toEqual(2);
      expect(list.head.next().next().next().next().next().get()).toEqual(67);
    });

    it('should set the length of the new list correctly', function() {
      var result = list.removeHead();
      expect(result.length).toEqual(5);
    });

  });

  describe('findNode', function() {

    var list;
    beforeEach(function() {
      list = iList(1,5,3,8,7,2,67,23,612,63,46,7,442,60,4,72,9,22,34);
    });

    it('should return the node with the matching value', function() {

      var predicate = function(x) { return x > 100 };

      var nodeOption = list.findNode(predicate);
      expect(nodeOption instanceof optionPkg.Some).toEqual(true);
      var node = nodeOption.get();
      expect(node.get()).toEqual(612);
      expect(node.next().get()).toEqual(63);

    });

    it('should return the first matching node', function() {

      var predicate = function(x) { return x === 7 };

      var nodeOption = list.findNode(predicate);
      expect(nodeOption instanceof optionPkg.Some).toEqual(true);
      var node = nodeOption.get();
      expect(node.get()).toEqual(7);
      expect(node.next().get()).toEqual(2);
      expect(node.next().next().get()).toEqual(67);

    });

    it('should handle a list which doesn\'t include the value', function() {

      var predicate = function(x) { return x === 2837627; }

      var node = list.findNode(predicate);
      expect(node instanceof optionPkg.None).toEqual(true);
    });

    it('should handle an empty list', function() {
      var l = new iList();
      var predicate = function(x) { return x === 42; };
      var result = l.findNode(predicate);
      expect(result instanceof optionPkg.None).toEqual(true);
    });

    it('can return the last node in the list', function() {

      var l = new iList(1,2,3,4,5,6,7,8,9,10);
      var predicate = function(x) { return x > 9 };

      var nodeOption = l.findNode(predicate);
      expect(nodeOption instanceof optionPkg.Some).toEqual(true);
      var node = nodeOption.get();
      expect(node.get()).toEqual(10);
    });

  });

  describe('foldLeft', function() {

    var list;
    beforeEach(function() {
      list = iList(1,5,3,8,7,2,67,23,612,63,46,7,442,60,4,72,9,22,34);
    });

    it('should be a curried function', function() {
      expect(typeof list.foldLeft(0)).toEqual('function');
    });

    it('should aggregate the values with the passed function', function() {
      var result = list.foldLeft(0)(function(acc, x) { return acc + x;});
      expect(result).toEqual(1487);
    });

    it('should handle an empty list', function() {
      var l = new iList();
      var result = l.foldLeft(42)(function(acc, x) { return acc + x; });
      expect(result).toEqual(42);
    });

  });

  describe('reverse', function() {

    var list;
    beforeEach(function() {
      list = iList(1,2,3,4,5,6,7,8);
    });

    it('should properly reverse the list', function() {
      var rev = list.reverse();
      expect(rev.head.get()).toEqual(8);
      expect(rev.head.next().get()).toEqual(7);
      expect(rev.head.next().next().get()).toEqual(6);
      expect(rev.head.next().next().next().get()).toEqual(5);
      expect(rev.head.next().next().next().next().get()).toEqual(4);
      expect(rev.head.next().next().next().next().next().get()).toEqual(3);
      expect(rev.head.next().next().next().next().next().next().get()).toEqual(2);
      expect(rev.head.next().next().next().next().next().next().next().get()).toEqual(1);
      expect(rev.head.next().next().next().next().next().next().next().next()).toEqual(null);
    });

    it('should leave the original list unchanged', function() {
      var result = list.reverse();
      expect(list.head.get()).toEqual(1);
      expect(list.head.next().get()).toEqual(2);
      expect(list.head.next().next().get()).toEqual(3);
      expect(list.head.next().next().next().get()).toEqual(4);
      expect(list.head.next().next().next().next().get()).toEqual(5);
      expect(list.head.next().next().next().next().next().get()).toEqual(6);
      expect(list.head.next().next().next().next().next().next().get()).toEqual(7);
      expect(list.head.next().next().next().next().next().next().next().get()).toEqual(8);
      expect(list.head.next().next().next().next().next().next().next().next()).toEqual(null);

    });

    it('should work on an empty list', function() {
      var l = new iList();
      var rev = l.reverse();
      expect(rev.head).toEqual(null);
    });

    it('should set the length of the new list', function() {
      var result = list.reverse();
      expect(result.length).toEqual(8);
    });

  });

  describe('reverseMap', function() {

    var list;
    beforeEach(function() {
      list = new iList(1,3,5,7);
    });

    it('should apply the function and reverse the list', function() {

      var result = list.reverseMap(function(x) {return 2 * x - 1});
      expect(result.toArray()).toEqual([13,9,5,1]);

    });

    it('should set the length on the new list', function() {
      var result = list.reverseMap(function(x) {return x - 1; });
      expect(result.length).toEqual(4);
    });

    it('should not modify the source list', function() {

      var result = list.reverseMap(function(x) { return x - 1});
      expect(list.toArray()).toEqual([1,3,5,7]);
      expect(result.toArray()).toEqual([6,4,2,0])

    });

    it('should work on an empty list', function() {
      var l = new iList();
      var result = l.reverseMap(function(x) {return x;})
      expect(result.head).toEqual(null);
    });




  });

  xdescribe('append', function() {

    var list;
    beforeEach(function() {
      list = iList(1,2,3,4);
    });

    it('appends two lists correctly', function() {
      var other = iList('a','b','c','d');
      var combined = list.append(other);
      expect(combined.head.get()).toEqual(1);
      expect(combined.head.next.get()).toEqual(1);
      expect(combined.head.next.next.get()).toEqual(1);
      expect(combined.head.next.next.next.get()).toEqual(1);
      expect(combined.head.next.next.next.next.get()).toEqual('a');
      expect(combined.head.next.next.next.next.next.get()).toEqual('b');
      expect(combined.head.next.next.next.next.next.next.get()).toEqual('c');
      expect(combined.head.next.next.next.next.next.next.next.get()).toEqual('d');
      expect(combined.head.next.next.next.next.next.next.next.next).toEqual(null);
    });

    it('should set the length of the new list correctly', function() {});


  });

  describe('insertAfter', function() {

    it('should properly insert the list', function() {
      var l = new iList(1,2,3,4,5);

      var predicate = function(x) { return x === 3; };

      var nodeOption = l.findNode(predicate);
      var node = nodeOption.get();
      var inserted = l.insertAfter(node, 'a');
      expect(inserted.head.get()).toEqual(1);
      expect(inserted.head.next().get()).toEqual(2);
      expect(inserted.head.next().next().get()).toEqual(3);
      expect(inserted.head.next().next().next().get()).toEqual('a');
      expect(inserted.head.next().next().next().next().get()).toEqual(4);
      expect(inserted.head.next().next().next().next().next().get()).toEqual(5);
      expect(inserted.head.next().next().next().next().next().next()).toEqual(null);
    });

    it('should not insert in an empty list', function() {
      var l = new iList();
      var inserted = l.insertAfter(null, 54);
      expect(inserted.head).toEqual(null);
    });

    it('should not insert if the provided nore is not found', function() {
      var l = new iList(1,2,3,4,5);

      var predicate = function(x) { return x === 15; };

      var nodeOption = l.findNode(predicate);
      var node = nodeOption.get();
      var inserted = l.insertAfter(node, 'a');
      expect(inserted.toArray()).toEqual([1,2,3,4,5]);
    });


    it('should set the length property', function() {
      var l = new iList(1,2,3,4,5);

      var predicate = function(x) { return x === 3; };

      var nodeOption = l.findNode(predicate);
      var node = nodeOption.get();
      var inserted = l.insertAfter(node, 'a');
      expect(inserted.length).toEqual(6);
    });

    it('should append the new value if the queried node is the last one', function() {

      var list = new iList(1,2,3,4,5);
      var predicate = function(x) { return x === 5 };

      var nodeOption = list.findNode(predicate);
      var node = nodeOption.get();
      var inserted = list.insertAfter(node, 'a');

      expect(inserted.toArray()).toEqual([1,2,3,4,5,'a']);
    });

    it('should not affect the original list', function() {
      var l = new iList(1,2,3,4,5);

      var predicate = function(x) { return x === 3; };

      var nodeOption = l.findNode(predicate);
      var node = nodeOption.get();
      var inserted = l.insertAfter(node, 'a');
      expect(inserted.head.get()).toEqual(1);
      expect(inserted.head.next().get()).toEqual(2);
      expect(inserted.head.next().next().get()).toEqual(3);
      expect(inserted.head.next().next().next().get()).toEqual('a');
      expect(inserted.head.next().next().next().next().get()).toEqual(4);
      expect(inserted.head.next().next().next().next().next().get()).toEqual(5);
      expect(inserted.head.next().next().next().next().next().next()).toEqual(null);

      expect(l.head.get()).toEqual(1);
      expect(l.head.next().get()).toEqual(2);
      expect(l.head.next().next().get()).toEqual(3);
      expect(l.head.next().next().next().get()).toEqual(4);
      expect(l.head.next().next().next().next().get()).toEqual(5);
      expect(l.head.next().next().next().next().next()).toEqual(null);

    });

  });

  describe('processing chains', () => {

    it('should properly apply a sequence of methods', () => {
      const initialValues = [1,45,14,28,154,117,58,91,67,98,23];
      var l = iList().fromArray(initialValues)
        .filter(x => x % 2 === 0) // [14,28,154,58,98]
        .map(x => x / 2)          // [7,14,77,29,49]
        .dropWhile(x => x < 70);  // [77,29,49]
      var l2 = l.foldLeft(100)((acc, curr) => acc + curr); // 100 + 155

      expect(l2).toEqual(255);
      expect(l.length).toEqual(3);
    });
  });
});
