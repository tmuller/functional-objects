/**
 * Description of code functionality
 */

'use strict';

var pkg = require('../lib/option');
var Option = pkg.Option;
var None = pkg.None;
var Some = pkg.Some;

describe('Option', function() {

  describe('constructor', function() {

    it('should return a "Some" without the "new" keyword', function() {
      var option = Option('test');
      expect(option.get()).toEqual('test');
      expect(option instanceof Some).toEqual(true);
    });

    it('should return a "Some" with a scalar value', function() {
      var tries = [42, 'some-string', [1,2,3,4], {}];
      var t, option;

      while (t = tries.shift()) {
        option = new Option(t);
        expect(option instanceof Some).toEqual(true);
        expect(option.get()).toEqual(t);
      }
    });

    it('should return a "Some" with empty string', function() {
      var option = new Option('');
      expect(option instanceof Some).toEqual(true);
      expect(option.get()).toEqual('');
    });

    it('should return a "Some" with empty the number zero', function() {
      var option = new Option(0);
      expect(option instanceof Some).toEqual(true);
      expect(option.get()).toEqual(0);
    });

    it('should return a "None" if a null or undefined variable is submitted', function() {
      var option = new Option(null);
      expect(option instanceof None).toEqual(true);
      expect(option.get()).toEqual(null);

      var a = {};
      option = new Option(a.test);
      expect(option instanceof None).toEqual(true);
      expect(option.get()).toEqual(null);
    });

    it('should return a "Some" if a passed function executes successfully', function() {
      var tstFct = function() { return 42;};
      var option = new Option(tstFct);
      expect(option instanceof Some).toEqual(true);
      expect(option.get()).toEqual(42);
    });

    it('should return a "None" of a passed function throws an exception', function() {
      var tstFct = function() { throw new TypeError('this is an exception')};
      var option = new Option(tstFct);
      expect(option instanceof None).toEqual(true);
      expect(option.get()).toEqual(null);
    });

  });

});

describe('Some', function() {

  describe('constructor', function() {

    it('should be instantiated without the "new" keyword', function() {
      var some = Some(42);
      expect(some instanceof Some).toEqual(true);
      expect(some.get()).toEqual(42);
    });

    it('should return a "Some" with a scalar value', function() {
      var tries = [42, 'some-string', [1,2,3,4], {}];
      var t, option;

      while (t = tries.shift()) {
        option = new Some(t);
        expect(option instanceof Some).toEqual(true);
        expect(option.get()).toEqual(t);
      }
    });

    it('should return a "Some" with empty string', function() {
      var option = new Some('');
      expect(option instanceof Some).toEqual(true);
      expect(option.get()).toEqual('');
    });

    it('should return a "Some" with empty the number zero', function() {
      var option = new Some(0);
      expect(option instanceof Some).toEqual(true);
      expect(option.get()).toEqual(0);
    });

    it('should return a "None" if a null or undefined variable is submitted', function() {
      var option = new Some(null);
      expect(option instanceof None).toEqual(true);
      expect(option.get()).toEqual(null);

      var a = {};
      option = new Some(a.test);
      expect(option instanceof None).toEqual(true);
      expect(option.get()).toEqual(null);
    });

    it('should return a "Some" if a passed function executes successfully', function() {
      var tstFct = function() { return 42;};
      var option = new Some(tstFct);
      expect(option instanceof Some).toEqual(true);
      expect(option.get()).toEqual(42);
    });

    it('should return a "None" of a passed function throws an exception', function() {
      var tstFct = function() { throw new TypeError('this is an exception')};
      var option = new Some(tstFct);
      expect(option instanceof None).toEqual(true);
      expect(option.get()).toEqual(null);
    });

    it('should return a "None" if a passed function returns null', function() {
      var option = new Some(function() { return null });
      expect(option instanceof None).toEqual(true);
      expect(option.get()).toEqual(null);
    });

    it('should return a "None" if a passed function returns undefined', function() {

      var option = new Some(function() {
        var a = {};
        return a.test
      });
      expect(option instanceof None).toEqual(true);
      expect(option.get()).toEqual(null);

    });

  });

  describe('map()', function() {

    it('should correctly apply the provided function to the value', function() {
      var some = new Some(42);
      var result = some.map(function(x) {return x / 2});
      expect(result instanceof Some).toEqual(true);
      expect(result.get()).toEqual(21);
      expect(some === result).toEqual(false);
    });

    it('should be able to map over a contained array', function() {
      var original = Some([1,2,3,4,5]);
      var result = original.map(function(x) { return x.map(function(k) {return 2 * k}) });
      expect(result instanceof Some).toEqual(true);
      expect(result.get()).toEqual([2,4,6,8,10]);
    });

    it('should return a "None" if applying the function fails', function() {
      var original = Some({test: true});
      var result = original.map(function(x) {return x.noExist.thisFails});
      expect(result instanceof None).toEqual(true);
      expect(result.get()).toEqual(null);
    });

  });

  describe('flatMap()', function() {

    it('should return a None if the function return a None', function() {
      var orig = Some(42);
      var testFct = function(x) {
        return new None();
      };
      var result = orig.flatMap(testFct);
      expect(result.get()).toEqual(null);
      expect(result instanceof None).toEqual(true);
    });

    it('should unwrap a Some when the function returns a Some', function() {
      var orig = Some(42);
      var testFct = function(x) {
        return new Some(x * 2);
      };
      var result = orig.flatMap(testFct);
      expect(result instanceof Some).toEqual(true);
      expect(result.get()).toEqual(84);
    });

    it('should return a new Some if the function returns a regular value', function() {
      var orig = Some(42);
      var testFct = function(x) {
        return x * 3 / 2;
      };
      var result = orig.flatMap(testFct);
      expect(result instanceof Some).toEqual(true);
      expect(result.get()).toEqual(63);
    });

    it('should return a None if the function returns a falsy value', function() {
      var orig = Some(42);
      var testFct = function(x) {
        return null;
      };
      var result = orig.flatMap(testFct);
      expect(result instanceof None).toEqual(true);
      expect(result.get()).toEqual(null);
    });

    it('should return a None if the passed function throws an exception', function() {
      var orig = Some(42);
      var testFct = function(x) {
        throw new Error('sample error');
      };
      var result = orig.flatMap(testFct);
      expect(result instanceof None).toEqual(true);
      expect(result.get()).toEqual(null);
    });

  });

  describe('getOrElse()', function() {

    it('should return the value of the Option', function() {
      var orig = Some(42);
      var result = orig.getOrElse('a string!');
      expect(result).toEqual(42);
    });

  });

  describe('filter()', function() {

    it('should return the same object if the filter passes', function() {
      var orig = new Some(42);
      var result = orig.filter(function(x) {return x % 2 === 0});
      expect(result instanceof Some).toEqual(true);
      expect(result === orig).toEqual(true);
      expect(result.get()).toEqual(42);
    });

    it('should return a None object if the filter fails', function() {
      var orig = new Some(42);
      var result = orig.filter(function(x) {return x > 1000});
      expect(result instanceof None).toEqual(true);
      expect(result.get()).toEqual(null);
    });

    it('should return a None if the application of the method fails', function() {
      var orig = Some({test:true});
      var result = orig.filter(function(x) { return x.nothing.thisFails === 534})
      expect(result instanceof None).toEqual(true);
      expect(result.get()).toEqual(null);
    });

    it('should return a None if the passed function throw an exception', function() {
      var orig = Some('this is a string');
      var result = orig.filter(function(x) {throw new TypeError('this will not work')});
      expect(result instanceof None).toEqual(true);
      expect(result.get()).toEqual(null);
    });

  });

  describe('orElse()', function() {

    it('should return the value of the Option', function() {
      var orig = Some(42);
      var result = orig.orElse(function() {return 'a string!'});
      expect(result).toEqual(42);
    });
  });

  describe('isEmpty()', function() {
    it('should return false', function() {
      var orig = Some(42);
      expect(orig.isEmpty()).toEqual(false);
    });
  })

});

describe('None', function() {

  it('should be instantiated without a "new" keyword', function() {
    var none = None();
    expect(none instanceof None).toEqual(true);
    expect(typeof none.get).toEqual('function');
    expect(none.get()).toEqual(null);
  });

  it('should be instantiated with a "new" keyword', function() {
    var none = new None();
    expect(none instanceof None).toEqual(true);
    expect(typeof none.get).toEqual('function');
    expect(none.get()).toEqual(null);
  });

  it('map() should return object', function() {
    var none = new None();
    var result = none.map(function(x) {return x / 2});
    expect(result instanceof None).toEqual(true);
    expect(result.get()).toEqual(null);
    expect(none === result).toEqual(true);
  });

  it('flatMap() should return the same None', function() {
    var orig = new None();
    var testFct = function(x) {
      return x * 2;
    };
    var result = orig.flatMap(testFct);
    expect(result instanceof None).toEqual(true);
    expect(result === orig).toEqual(true);

  });

  it('getOrElse() should return the value else value', function() {
    var orig = None();
    var result = orig.getOrElse('a string!');
    expect(result).toEqual('a string!');
  });

  it('filter() should return the same object', function() {
    var orig = new None();
    var result = orig.filter(function(x) {return x % 2 === 0});
    expect(result instanceof None).toEqual(true);
    expect(result === orig).toEqual(true);
    expect(result.get()).toEqual(null);
  });

  it('getOrElse() should return the value of the function', function() {
    var orig = None();
    var result = orig.orElse(function() {return 'a string!'});
    expect(result).toEqual('a string!');
  });

  it('isEmpty() should return true', function() {
    var orig = new None();
    expect(orig.isEmpty()).toEqual(true);
  });


});
