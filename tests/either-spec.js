/**
 * Description of code functionality
 */

'use strict';

const pkg = require('../lib/either');
const OptionPkg = require('../lib/option');
const Either = pkg.Either;
const Right = pkg.Right;
const Left = pkg.Left;
const Some = OptionPkg.Some;
const None = OptionPkg.None;

describe('Either', function () {

  describe('constructor', function () {

    it('should be instantiable', () => {
      const either = new Either('a value');
      expect(either).not.toBe(null);
      expect(either.get()).toBe('a value');
    });

    it('should be instantiable without the "new" keyword', () => {
      const either = Either('a value');
      expect(either).not.toBe(null);
      expect(either.get()).toBe('a value');
    });

    it('should return a Right if instantiated with a value', () => {
      const either = new Either('a value');
      expect(either).not.toBe(null);
      expect(either instanceof Right).toEqual(true);
      expect(either.get()).toEqual('a value');
    });

    it('should return a Left if instantiated with a null', () => {
      const either = new Either(null);
      expect(either).not.toBe(null);
      expect(either instanceof Left).toEqual(true);
      expect(either.get() instanceof Error).toBe(true);
      expect(either.get().message).toEqual('NULL encountered');
    });

    it('should return a Left if instantiated with an Exception', () => {
      const err = new Error('sample error')
      const either = new Either(err);
      expect(either).not.toBe(null);
      expect(either instanceof Left).toEqual(true);
      expect(either.get() instanceof Error).toBe(true);
      expect(either.get().message).toEqual('sample error');
    });

    it('should return a Left if instantiated without a value', () => {
      const either = new Either();
      expect(either).not.toBe(null);
      expect(either instanceof Left).toEqual(true);
      expect(either.get() instanceof Error).toBe(true);
      expect(either.get().message).toEqual('undefined encountered');
    });

    it('should be instantiated without the new keyword', () => {
      const either = Either('the value');
      expect(either).not.toBe(null);
      expect(either instanceof Right).toEqual(true);
    });

    it('should return a Right when passed an executed function', () => {
      const fct = x => x * 3;
      const either = Either(fct(14));

      expect(either instanceof Right).toBe(true);
      expect(either.get()).toBe(42);
    });

    it('should return a Left if instantiated with unexecuted function', () => {
      const arg = () => {}
      const either = new Either(arg);
      expect(either instanceof Left).toEqual(true);
      expect(either.get() instanceof Error).toBe(true);
      expect(either.get().message).toEqual('Functions are not a valid value');
    });

  });

  describe('Right', () => {

    const sampleValue = 'some value here';

    it('should be creatable without a new keyword', () => {
      const right = Right(sampleValue);
      expect(right instanceof Right).toBe(true);
    });

    it('should have a getter to retrieve its value', () => {
      const either = Right('the value');
      expect(either.get()).toEqual('the value');
    });

    it('should return a Right with a scalar value stored', () => {
      const options = [143, 'a string', {}, [1, 2, 3, 4]];
      for (const o in options) {
        const either = Right(o);
        expect(either instanceof Right).toBe(true);
        expect(either.get()).toEqual(o);
      }
    });

    it('should return a Right with valid values equating to boolean false', () => {
      const options = [0, false, ''];
      for (const v in options) {
        const right = Right(v);
        expect(right instanceof Right).toBe(true);
      }
    });

    it('should return a Left for a value of null', () => {
      const notRight = Right(null);
      expect(notRight instanceof Left).toBe(true);
    });

    it('should return a Left without a value', () => {
      const emptyRight = Right();
      expect(emptyRight instanceof Left).toBe(true);
    });

    it('should return a Left if a function is passed', () => {
      const parameter = () => {}
      const shouldBeLeft = Right(parameter);
      expect(shouldBeLeft instanceof Left).toBe(true);
      expect(shouldBeLeft.get() instanceof Error).toBe(true);
      expect(shouldBeLeft.get().message).toBe('Functions are not a valid value')
    });

    it('should return a Left if instantiated with a value of undefined', () => {
      const either = new Right();
      expect(either).not.toBe(null);
      expect(either instanceof Left).toEqual(true);
      expect(either.get() instanceof Error).toBe(true);
      expect(either.get().message).toEqual('undefined encountered');
    });
  });

  describe('Operator', () => {

    describe('mapRight()', () => {

      it('should apply the passed function and return a new instance with the result', () => {
        const right = new Right(42);
        const operator = value => value * 2;

        const result = right.mapRight(operator);

        expect(result instanceof Right).toBe(true);
        expect(result.get()).toEqual(84);
        expect(Object.is(result, right)).toBe(false);
      });

      it('should return a Left if the operator function throws an exception', () => {
        const right = Right(42);
        const operator = () => { throw new Error('SOME ERROR') };

        const result = right.mapRight(operator);

        expect(result instanceof Left).toBe(true);
        expect(result.get().message).toEqual('SOME ERROR');
      });

      it('should be a NO-OP if the Either is a Left', () => {
        const left = Left(42);
        const operator = a => a * 2;

        const result = left.mapRight(operator);

        expect(result.get()).toEqual(42);
        expect(Object.is(result, left)).toBe(true);
      });

      it('should return a Right if the computation result is 0', () => {
        const fct = x => 0;
        const right = Right(42);

        const result = right.mapRight(fct);

        expect(result instanceof Right).toBe(true);
        expect(result.get()).toEqual(0);
      });

      it('should return a Left if the operator returns null', () => {
        const fct = x => null;
        const right = Right(42);

        const result = right.mapRight(fct);

        expect(result instanceof Left).toBe(true);
        expect(result.get()).toEqual('NULL value');
      });

      it('should return a Left with the original error message on operator failure', () => {
        const fct = x => { throw new Error('error in operator function')};
        const left = Left('The original error');

        const result = left.mapRight(fct);

        expect(result instanceof Left).toBe(true);
        expect(result.get()).toEqual('The original error');
      });
    });

    describe('isLeft()', () => {

      it('should return true if the instance is a Left', () => {
        const left = Left(new Error("default error"));

        expect(left.isLeft()).toBe(true);
      });

      it('should return false if the instance is a Right', () => {
        const right = Right(42);
        expect(right.isLeft()).toBe(false);
      });
    });

    describe('flatMap()', () => {

      it('should return an unnested Right if passed operator returns a Right', () => {

        const targetInstance = new Right(42);
        const fct = x => Right(84 + x);

        const result = targetInstance.flatMap(fct);

        expect(result instanceof Right).toBe(true);
        expect(result.get()).toEqual(126);
      });

      it('should flatten nested Rights into one', () => {
        const targetInstance = Right(42);
        const fct = x => Right(Right(Right(15 + x)));

        const result = targetInstance.flatMap(fct);

        expect(result instanceof Right).toBe(true);
        expect(result.get()).toEqual(57);
      });

      it('should return a Right if the applied method does not return a Monad', () => {

        const targetInstance = Right(42);
        const fct = x => x + 96;

        const result = targetInstance.flatMap(fct);

        expect(result instanceof Right).toBe(true);
        expect(result.get()).toEqual(138);
      });

      it('should return itself if called on a Left', () => {

        const exception = new Error('some error msg here');
        const targetInstance = Left(exception);
        const fct = x => x + 'some text here';

        const result = targetInstance.flatMap(fct);

        expect(result instanceof Left).toBe(true);
        expect(result).toBe(targetInstance);
      });

      it('should return a Left if the operator throws an error', () => {
        const targetInstance = Right(42);
        const fct = x => {
          const f = () => { throw new Error('some error msg here'); }
          f();
        }

        const result = targetInstance.flatMap(fct);

        expect(result instanceof Left).toBe(true);
        expect(result.get() instanceof Error).toBe(true);
        expect(result.get().message).toBe('some error msg here');
      });

      it('should return a Left if the operator returns a Left', () => {
        const targetInstance = Right(42);
        const fct = x => Left('There was an error');

        const result = targetInstance.flatMap(fct);

        expect(result instanceof Left).toBe(true);
        expect(result.get()).toBe('There was an error');
      });

      xit('should return a Left if the result is null', () => {

      });
    });

    describe('flatten()', () => {

      it('should flatten nested Either.Right() to one level', () => {
        const targetInstance = Right(42);
        const fct = x => Right(Right(15 + x));

        const result = targetInstance.mapRight(fct);
        const flattened = result.flatten();

        expect(flattened instanceof Right).toBe(true);
        expect(flattened.get()).toEqual(57);
      });

      xit('should return an unnested Left', () => {

      });

      it('should return a Left if innermost result is a Left for nested Either', () => {
        const targetInstance = Right(Left("There was an error"));

        const result = targetInstance.flatten();

        expect(result instanceof Left).toBe(true);
        expect(result.get()).toBe('There was an error');
      });

    });

    describe('getOrElse()', () => {

      it('should return the encapsulated value if Right', () => {
        const testInstance = Right(42);
        const result = testInstance.getOrElse();

        expect(result).toBe(42);
      });

      it('should return a passed default value if instance is a Left', () => {
        const testInstance = Left(42);
        const result = testInstance.getOrElse("correct");

        expect(result).toBe("correct");
      });
    });

    describe('toOption()', () => {

      it('should return a Some for a Right instance', () => {
        const either = Right(42);

        const option = either.toOption();

        expect(option instanceof Some).toEqual(true);
        expect(option.get()).toEqual(42)
      });

      it('should return a None for a Left instance', () => {
        const either = Left(42);

        const option = either.toOption();

        expect(option instanceof None).toEqual(true);
      });
    });

  });

  xdescribe('Examples', () => {

    it('should be chainable', () => {

      const leftOrRight = msg => value =>  {
        return (Math.trunc(Math.random() * 100000) % 5 === 0 )
          ? Left(new Error(msg))
          : Right(value);
      };

      const double = x => x * 2;
      const iDoApiCalls = leftOrRight('I just felt like it');

      const result = leftOrRight('I do not like numbers divisible by 5')(42)
        .mapRight(double)
        .flatMap(iDoApiCalls)

      console.log('I am a', result.isLeft() ? 'Left' : 'Right');
      console.log('My value is:', result.getOrElse('There was an error'));
      console.log('Error message:', result.get().message);

    });
  });
});
