/**
 * Implements an immutable list
 */

'use strict';

var optionPkg = require('./option');

function cloneNodeValue(val) {

  var type = Object.prototype.toString.call(val).slice(8, -1);
  switch (type) {

    case 'Array':
      var l = val.length;
      var clone = [];
      for (var i=0; i < l; i++) {
        clone[i] = cloneNodeValue(val[i]);
      }
      return clone;
      break;

    case 'Object':
      var k, keys = Object.keys(val), obj = {};
      while (k = keys.shift()) {
        obj[k] = cloneNodeValue(val[k]);
      }
      return obj;
      break;

    default:
      return val;
      break;
  }
}

var Node = function(val) {

  var nodeValue = val;
  var nextNode = null;

  this.get = function(skipClone) {
    return (skipClone) ? nodeValue : cloneNodeValue(nodeValue);
  };
  this.bind = function(node) {
    if (nextNode === null) nextNode = node;
  };
  this.next = function() { return nextNode; };
};


var ImmutableList = function() {

  if (this instanceof ImmutableList) {
    this.head = null;
    this.last = null;
    this.length = arguments.length;

    var l = arguments.length, i,
      nn;

    // Take all the arguments and put them into the list in the order of their appearance.
    for (i = l-1; i > -1; i--) {
      nn = new Node(arguments[i]);
      if (i === l-1) this.last = nn;
      nn.bind(this.head);
      this.head = nn;
    }
  } else {
    var param = Array.prototype.slice.call(arguments);
    param.unshift('');
    return new (Function.prototype.bind.apply(ImmutableList, param));
  }
};

var ListProto = {

  /**
   * Adds a new
   *
   * @param {number} el
   * @returns {ImmutableList}
   */
  insertHead: function(el) {

    var nl = new ImmutableList();
    nl.head  = new Node(el);
    nl.head.bind(this.head);
    nl.length = this.length + 1;
    return nl;
  },

  /**
   * Prints a comma-separated string of the list's values to the console.
   */
  toString: function() {

    var result = '';

    function recurse(node) {
      if (node.next() !== null) {
        return result + node.get(true) + ',' + recurse(node.next());
      } else {
        return result + node.get(true);
      }
    }

    return recurse(this.head);
  },

  map: function(fct) {

    function iterate(node, nl, fct) {

      if (nl.head === null) {
        nl.head = new Node(fct(node.get()));
        nl.last = nl.head;
        iterate(node.next(), nl, fct);
      } else {
        var nd = new Node(fct(node.get()));
        nl.last.bind(nd);
        nl.last = nd;
        if (node.next() === null) {
          return nl;
        } else {
          iterate(node.next(), nl, fct);
        }
      }
    }

    var newList = new ImmutableList();
    if (this.head !== null) {
      iterate(this.head, newList, fct);
      newList.length = this.length;
    }
    return newList;

  },

  filter: function(f) {

    // Tail recursive
    var iterate = function(node, newList, fct) {
      if (node === null) return newList;
      else {
        if (true === fct(node.get())) {
          if (newList.head === null) {
            newList.head = new Node(node.get());
            newList.last = newList.head;
          } else {
            var nd = new Node(node.get());
            newList.last.bind(nd);
            newList.last = nd;
          }
          newList.length += 1;
          return iterate(node.next(), newList, fct);
        } else {
          return iterate(node.next(), newList, fct);
        }
      }
    };

    return iterate(this.head, new ImmutableList(), f);
  },

  drop: function(num) {

    var findNewHeadNode = function(headNode, n) {
      if (headNode === null) return null;
      else if (n === 0) return headNode;
      else return findNewHeadNode(headNode.next(), n - 1);
    };

    var newList = new ImmutableList();
    if (this.length - num > 0) {
      newList.last = this.last;
      newList.length = this.length - num;
      newList.head = findNewHeadNode(this.head, num);
    }
    return newList;
  },

  dropWhile: function(fct) {

    var findHeadnode = function(node, f) {
      if (node.node === null) return {node: null, c: node.c};
      else if (false === f(node.node.get(true))) return node;
      else return findHeadnode.call(this, {'node': node.node.next(), c: node.c+1}, f);
    };

    var newList = new ImmutableList();
    var newHead = findHeadnode.call(this, {node: this.head, c: 0}, fct);
    newList.head = newHead.node;
    if (newHead.node !== null) {
      newList.last = this.last;
      newList.length = this.length - newHead.c;
    }
    return newList;

  },

  exists: function(fct) {
    var node = this.findNode(fct);
    return node.get(true) !== null;
  },

  /**
   * Applies a function to every element in the list and returns a new list with the modified
   * nodes.
   *
   * @param {function} fct  The function to apply to every element in the list
   *
   * @returns {ImmutableList}
   */
  forEach: function(fct) {

    function recurse(h) {
      if (h !== null) {
        fct(h.get());
        recurse(h.next());
      }
    }

    recurse(this.head);
  },

  /**
   * Returns a new list with the first element removed.
   *
   * @returns {ImmutableList}
   */
  removeHead: function() {
    var nl = new ImmutableList();
    nl.head = this.head.next();
    nl.last = this.last;
    nl.length = this.length - 1;
    return nl;
  },

  /**
   * Searches the list and returns the first the first node which matches the value requested.
   *
   * @param val
   * @returns {Node}
   */
  findNode: function(fct) {

    function iterate(h) {
      if (h === null) return optionPkg.None();
      else if (fct(h.get(true))) return optionPkg.Some(h);
      else return iterate(h.next());
    }

    return iterate(this.head);
  },

  /**
   * Reduces the list to a single value
   *
   * @param {number}   initVal  The initial value for the fold
   * @param {function} fct      The function to aggregate the data
   *
   * @returns {*}
   */
  foldLeft: function(initVal) {

    var self = this;

    return function(fct) {

      function iterate(cVal, node) {
        if (node === null) return cVal;
        else return iterate(fct(cVal, node.get()), node.next());
      }

      return iterate(initVal, self.head);
    }
  },

  /**
   * Reverses the list
   *
   * @returns {ImmutableList}
   */
  reverse: function() {

    function iterate(node, newList) {
      if (node === null) return newList;
      else {
        var rev = newList.insertHead(node.get());
        return iterate(node.next(), rev);
      }
    }

    var rev = new ImmutableList();
    rev.last = this.head;
    return iterate(this.head, rev);

  },

  reverseMap: function(fct) {

    var rev = new ImmutableList();

    function iterate(node, newList) {
      if (node === null) return newList;
      else {
        var rev = newList.insertHead(fct(node.get()));
        return iterate(node.next(), rev);
      }
    }

    rev.last = this.head;
    return iterate(this.head, rev);
  },

  toArray: function() {

    function iterate(arr, node) {

      if (node === null) return arr;
      else {
        arr.push(node.get());
        return iterate(arr, node.next());
      }
    }

    return iterate([], this.head);
  },

  /**
   * Inserts a new node after the given one. Returns a new list with the new node added.
   *
   * @param {Node|object} node
   * @param {*}           val
   *
   * @returns {ImmutableList}
   */
  insertAfter: function(node, val) {

    function traverse(nd, newList, insertVal) {
      if (nd === null) {
        return newList;
      } else if (nd === node) {
        var newNode = new Node(insertVal);
        var prevNode = new Node(nd.get());

        // Add the current node, as we want to insert AFTER
        newList.last.bind(prevNode);
        // link copy of current node to new node, INSERTING here
        prevNode.bind(newNode);
        // Link the new node to the rest of the list, which should be unaltered
        newNode.bind(nd.next());
        newList.last = this.last;
        newList.length = this.length + 1;
        return newList;
      } else {
        if (newList.head === null) {
          var nn = new Node(nd.get());
          newList.head = nn;
          newList.last = nn;
          return traverse.call(this, nd.next(), newList, insertVal);
        } else {
          var nn = new Node(nd.get());
          newList.last.bind(nn);
          newList.last = nn;
          return traverse.call(this, nd.next(), newList, insertVal);
        }
      }
    }

    var nl = new ImmutableList();
    return traverse.call(this, this.head, nl, val);
  },

  fromArray: function(arr) {
    if (Array.isArray(arr)) {
      return ImmutableList.apply(null, arr);
    } else {
      return new ImmutableList();
    }
  }
};

ImmutableList.prototype = ListProto;
ImmutableList.prototype.constructor = ImmutableList;

module.exports = ImmutableList;
