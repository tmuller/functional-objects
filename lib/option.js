/**
 * Implements an option object
 */

'use strict';

var Option = function(expression) {

  if (this instanceof Option) {
    if (typeof expression == 'function') {
      try {
        return new Some(expression())
      } catch (e) {
        return new None();
      }
    } else {
      if (expression || expression === 0 || expression === '') {
        return Some(expression);
      } else {
        return new None();
      }
    }
  } else {
    return new Option(expression);
  }
};

var OptionProto = {

  map: function(fct) {
    if (this.get() === null) return this;
    return new Some(function() { return fct(this.get()) }.bind(this));
  },

  flatMap: function(fct) {

    if (this.get() === null) return this;
    try {
      var applied = fct(this.get());
      if (applied instanceof None || applied instanceof Some) {
        return applied;
      } else {
        return new Option(applied);
      }
    } catch(e) {
      return new None();
    }
  },

  filter: function(fct) {
    if (this.get() === null) return this;
    try {
      if (fct(this.get())) {
        return this;
      } else {
        return new None();
      }
    } catch (e) {
      return new None();
    }
  },

  getOrElse: function(elseVal) {
    return (this.get() === null) ? elseVal : this.get();
  },

  orElse: function(fct) {
    return (this.get() === null) ? fct() : this.get();
  },

  isEmpty: function() {
    return this.get() === null;
  }

};

var None = function() {
  var value = null;

  if (!(this instanceof None)) {
    return new None();
  }

  this.get = function() { return value; };
};
None.prototype = OptionProto;
None.prototype.constructor = None;

var Some = function(expression) {

  var value = null;

  if (this instanceof Some) {
    if (typeof expression == 'function') {
      try {
        var evaluated = expression();
        if (evaluated || evaluated === 0 || evaluated === '') {
          value = evaluated;
        } else {
          return new None();
        }

      } catch (e) {
        return new None();
      }
    } else {
      if (expression || expression === 0 || expression === '') {
        value = expression;
      } else {
        return new None();
      }
    }
  } else {
    return new Some(expression);
  }

  this.get = function() { return value; };

};
Some.prototype = Object.assign({}, OptionProto);
Some.prototype.constructor = None;

Option.prototype = Object.assign({}, OptionProto);
Option.prototype.constructor = Option;

module.exports = {
  Option: Option,
  None: None,
  Some: Some
};
