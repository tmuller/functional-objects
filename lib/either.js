
'use strict'

const OptionPkg = require('../lib/option');
const Some = OptionPkg.Some;
const None = OptionPkg.None;

const Either = function(value) {
  if (!this instanceof Either) {
    return new Either(value);
  } else {
    if (value === null) return Left(new Error('NULL encountered'));
    else if (value instanceof Error) return Left(value);
    else if (typeof value == 'function') return Left(new Error('Functions are not a valid value'))
    else if (typeof value === 'undefined') {
      return Left(new Error('undefined encountered'));
    } else {
      return new Right(value);
    }
  }
}

const Right = function(expression) {
  if (!(this instanceof Right)) {
    return new Right(expression);
  } else {
    if (typeof expression == 'function') return Left(new Error('Functions are not a valid value'))
    else if (typeof expression === 'undefined') {
      return new Left(new Error('undefined encountered'));
    }
    else if (!expression && [0, ''].indexOf(expression) < 0) return Left(expression);
  }

  this.get = function() { return expression; }
}

const Left = function(error) {

  if (!(this instanceof Left)) {
    return new Left(error);
  }

  this.get = function() { return error; }
}

const EitherProto = {
  mapRight: function(mapFct) {
    try {
      if  (this instanceof Left) return this;
      else {
        const result = mapFct(this.get());
        if (result == null) return Left('NULL value');
        else return Right(result);
      }
    } catch(e) {
      return Left(e);
    }
  },

  isLeft: function() {
    return this instanceof Left
  },

  flatMap: function(operator) {
    try {
      if (this instanceof Left) return this;
      else {
        const result = operator(this.get());
        if (result instanceof Right || result instanceof Left) {
          return result.flatten();
        } else return Right(result);
      }
    } catch(e) {
      return Left(e);
    }
  },

  flatten: function() {
    if (this.get() instanceof Right || this.get() instanceof Left) {
      return this.get().flatten();
    } else {
      return (this instanceof Right) ? Right(this.get()) : Left(this.get());
    }
  },

  getOrElse: function(defaultValue) {
    if (this instanceof Right) return this.get();
    else return defaultValue;
  },

  toOption: function() {
    return (this instanceof Right) ? Some(this.get()) : None();
  }
}

Right.prototype = Object.assign({}, EitherProto)
Right.prototype.constructor = Right;

Left.prototype = Object.assign({}, EitherProto);
Left.prototype.constructor = Left;

module.exports = {
  Either: Either,
  Right: Right,
  Left: Left,
};

